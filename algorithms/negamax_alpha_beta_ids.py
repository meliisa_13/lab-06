from datetime import datetime
import operator
from random import shuffle
import time

from base.bot import HeuristicBot, A, G, H
from base.player import Player
from base.score import Score
from base.state import State
from base.heuristic import HeuristicScore
from base.action import Action
import math
from typing import Type


class NegamaxAlphaBetaIDS(HeuristicBot[A, G, H]):
    """
    A configurable MiniMax Alpha-Beta with depth limit player implemented using Negamax: https://www.wikiwand.com/en/Negamax
    
    Attributes
    -----------
    visited_nodes: int
        Counts how many game tree nodes have been visited by the algorithm
    explored_level: int
        Which was the maximal tree level explored in the last run
    explored_all: bool
        Whether the algorithm explored the whole tree
    pruned: int
        Counts how many times alpha/beta were used to prune the search tree
    move_timeout: int
        How much time we have for the move.
    heuristic: Heuristic
        A heuristic to estimate the state value.
    reorder_actions: bool
        Whether the algorithm should try to reorder actions.
    prune_branches: bool
        Whether the algorithm should use alpha/beta pruning
    """
    move_timeout: float
    reorder_actions: bool
    prune_branches: bool
    visited_nodes: int = 0
    explored_level: int = 0
    explored_all: bool = False
    pruned: int = 0

    def name(self) -> str:
        if self.prune_branches:
            return f"NegamaxAlphaBetaIDS_{self.heuristic_constructor.__name__}"
        else:
            return f"NegamaxIDS_{self.heuristic_constructor.__name__}"

    def __init__(self, heuristic_constructor: Type[H], move_timeout: float, reorder_actions: bool = True,
                 prune_branches: bool = True):
        super().__init__(heuristic_constructor)
        self.move_timeout = move_timeout
        self.reorder_actions = reorder_actions
        self.prune_branches = prune_branches

    def _choose_action(self, state: State) -> None:
        self.best_action = None
        self.pruned = 0
        self.visited_nodes = 0
        self.explored_level = 0
        self.explored_all = False

        start = datetime.now()

        def any_time_left() -> bool:
            return (datetime.now() - start).total_seconds() < self.move_timeout

        current_level = 1
        while not self.explored_all and any_time_left():
            self.explored_all = True
            self.best_action = self._alpha_beta(
                state, current_level, HeuristicScore.worst_possible(), HeuristicScore.best_possible(), self.player,
                self.best_action)[0]
            self.explored_level = current_level
            current_level += 1

    def _alpha_beta(self, state: State, depth: int, alpha: HeuristicScore, beta: HeuristicScore, player: Player,
                    last_winner: A | None = None) -> tuple[A | None, HeuristicScore]:
        """ Negamax function following https://www.chessprogramming.org/Alpha-Beta#Outside_the_Bounds

        Player acts like the `color` parameter from the article, but its logic is already included
        in the reward method of the Game class.
        It has a depth limit to prevent algorithm from breaking the timelimit.
        
        Parameters
        ----------
        state: State
            the current state in the game
        depth: int
            how deep we explore the tree from this state
        alpha: HeuristicScore
            current lower bound for the final score
        beta: HeuristicScore
            current upper bound for the final score
        player: Player
            the player that is supposed to move at the given state  
        last_winner: Action | None
            if available, it informs what action was chosen in the previous run (with the smaller depth limit)
            a naive way used to order the actions...  
        
        Returns
        -------
        tuple[Action | None, HeuristicScore]:
            - the first tuple element is the action, that player would choose at the state;
            it should be equal None if the state is terminal and there is no action to make
            - the second tuple element is the score got by the player by following the action
        """
        self.visited_nodes += 1  # to track the progress...

        # TODO:
        # 1) start with code from the negamax_alpha_beta_depth.py
        # 2) if the depth limit has been reached and the state is still not terminal, set `self.explored_all` to False
        # 3) call _reorder_actions to order the actions 
        #    - orderMoves from https://www.wikiwand.com/en/Negamax#Negamax_with_alpha_beta_pruning 
        raise NotImplementedError()

    def _reorder_actions(self, actions: list[A], last_winner: A | None):
        """A naive in-place method to order the actions"""
        if not self.reorder_actions:
            return actions
        if last_winner is None:
            return actions
        shuffle(actions)  # to make things a bit more interesting...

        # TODO:
        # Reorder the actions, so the `last_winner` is the first element
        raise NotImplementedError()

    def metric(self) -> str | None:
        if self.explored_all:
            return f"Explored the whole tree ({self.explored_level} levels):\n\t- visited {self.visited_nodes} nodes\n\t- pruned {self.pruned} branches"
        return f"Explored {self.explored_level} level of the tree:\n\t- visited {self.visited_nodes} nodes\n\t- pruned {self.pruned} branches"
