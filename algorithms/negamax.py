import operator

from base.bot import G, A, Bot
from base.player import Player
from base.score import Score
from base.state import State
from base.action import Action


class Negamax(Bot[G, A]):
    """
    A MiniMax player implemented using Negamax: https://www.wikiwand.com/en/Negamax
    
    Attributes:
    -----------
    visited_nodes: int
        Counts how many game tree nodes have been visited by the algorithm
    """
    visited_nodes: int

    def __init__(self):
        self.visited_nodes = 0
        super().__init__()

    def _choose_action(self, state: State) -> None:
        self.visited_nodes = 0
        self.best_action = self._negamax(state, self.player)[0]

    def _negamax(self, state: State, player: Player) -> tuple[A | None, Score]:
        """ Negamax function following https://www.wikiwand.com/en/Negamax#Negamax_base_algorithm

        Player acts like the `color` parameter from the wikipedia article, but its logic is already included
        in the reward method of the Game class.
        
        Parameters
        ----------
        state: State
            the current state in the game
        player: Player
            the player that is supposed to move at the given state    
        
        Returns
        -------
        tuple[Action | None, Score]:
            - the first tuple element is the action, that player would choose at the state;
            it should be equal None if the state is terminal and there is no action to make
            - the second tuple element is the score got by the player by following the action
        """
        self.visited_nodes += 1  # to track the progress...

        # TODO:
        # 1) check if state is terminal [1]
        #   - if so, return no action and a reward for the player [2]
        # 2) call negamax recursively[3] for every possible state following from the current one [4]
        #    just follow the wikipedia pseudocode and it should be fine:
        #    - the worst possible score is Score.Lost, no need for -infinity :D
        #    - the wikipedia code does not return the action, take it under consideration
        #   return both the chosen action and the best achievable score
        #
        # [1] method: `self.game.is_terminal_state`
        # [2] method: `self.game.reward`
        # [3] method: `player.opponent` return the opponent player (`-color` in wikipedia)
        # [3] methods: `self.game.actions_for` and `self.game.take_action`
        raise NotImplementedError()

    def metric(self) -> str | None:
        return f"Visited {self.visited_nodes} nodes in the game tree."
