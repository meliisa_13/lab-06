import operator

from base.bot import HeuristicBot, A, G, H
from base.heuristic import HeuristicScore
from base.player import Player
from base.score import Score
from base.state import State
from base.action import Action
import math
from typing import Type


class NegamaxAlphaBetaDepth(HeuristicBot[A, G, H]):
    """
    A MiniMax Alpha-Beta with depth limit player implemented using Negamax: https://www.wikiwand.com/en/Negamax
    
    Attributes
    -----------
    visited_nodes: int
        Counts how many game tree nodes have been visited by the algorithm
    pruned: int
        Counts how many times alpha/beta were used to prune the search tree
    max_depth: int
        Search limit for the algorithm
    heuristic: Heuristic
        A heuristic to estimate the state value.
    """
    visited_nodes: int
    pruned: int
    max_depth: int

    def name(self) -> str:
        return f"{type(self).__name__}_{self.max_depth}"

    def __init__(self, heuristic_constructor: Type[H], max_depth: int, ):
        super().__init__(heuristic_constructor)
        self.visited_nodes = 0
        self.pruned = 0
        self.max_depth = max_depth

    def _choose_action(self, state: State) -> None:
        self.visited_nodes = 0
        self.best_action = self._alpha_beta(
            state, self.max_depth, HeuristicScore.worst_possible(), HeuristicScore.best_possible(), self.player)[0]

    def _alpha_beta(self, state: State, depth: int, alpha: HeuristicScore, beta: HeuristicScore, player: Player) \
            -> tuple[A | None, HeuristicScore]:
        """ Negamax function following https://www.chessprogramming.org/Alpha-Beta#Outside_the_Bounds

        Player acts like the `color` parameter from the article, but its logic is already included
        in the reward method of the Game class.
        It has a depth limit to prevent algorithm from breaking the timelimit.
        
        Parameters
        ----------
        state: State
            the current state in the game
        depth: int
            how deep we explore the tree from this state
        alpha: HeuristicScore
            current lower bound for the final score
        beta: HeuristicScore
            current upper bound for the final score
        player: Player
            the player that is supposed to move at the given state    
        
        Returns
        -------
        tuple[Action | None, HeuristicScore]:
            - the first tuple element is the action, that player would choose at the state;
            it should be equal None if the state is terminal and there is no action to make
            - the second tuple element is the score got by the player by following the action
        """
        self.visited_nodes += 1  # to track the progress...

        # TODO:
        # 1) start with the alpha-beta - copy code from the negamax_alpha_beta.py
        # 2) add a depth limit condition when checking if the state is terminal
        #   - instead return the reward use the `self.heuristic(state, player)`
        # 
        #   HeuristicScore can be inverted (e.g., -score) and compared like a normal number.
        #   Check its short documentation in `base/heuristic.py`
        raise NotImplementedError()

    def metric(self) -> str | None:
        return f"Visited {self.visited_nodes} nodes in the game tree. Pruned {self.pruned} branches."
